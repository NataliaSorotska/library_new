﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaperLibrary.Cmd
{
    public sealed class Book : IComparable<Book>
    {
        public String Name { get; set; }

        public Author Author { get; set; }

        public UInt32 PageCount { get; set; }
        public Book()
        {
        }
        public Book(String name, Author author, UInt32 pageCount) : this()
        {
            if (name != null && author != null)
            {
                Name = name;
                Author = author;
                PageCount = pageCount;
            }
            else
            {
                throw new NullReferenceException("Please write name and author books");
            }
        }
        public Int32 CompareTo(Book other)
        {
            if (ReferenceEquals(this, other))
            {
                return 0;
            }
            if (ReferenceEquals(null, other))
            {
                return 1;
            }

            Int32 pageCountcomparison = PageCount.CompareTo(other.PageCount);

            return pageCountcomparison;
        }
        public override string ToString()
        {
            return Name;
        }
    }
}
