﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaperLibrary.Cmd
{
    public sealed class Author : ICountingBook, IComparable<Author>
    {
        public String FirstName { get; set; }

        public String LastName { get; set; }

        public String FullName { get; set; }

        public List<Book> Books { get; set; }
        public Author()
        {
            Books = new List<Book>();
        }
        public Author(String firstName, String lastName) : this()
        {
            this.FirstName = firstName;
            this.LastName = lastName;
            this.FullName = $"{firstName} {lastName}";
        }
        public Author(String firstName, String lastName, IEnumerable<Book> books)
           : this(firstName, lastName)
        {
            foreach (Book b in books)
            {
                this.Books.Add(b);
            }
        }
        public Int32 GetCountBook()
        {
            Int32 countBooks = this.Books.Count;
            return countBooks;
        }
        public Int32 CompareTo(Author other)
        {
            if (ReferenceEquals(this, other))
            {
                return 0;
            }
            if (ReferenceEquals(null, other))
            {
                return 1;
            }
            Int32 countBooksInThisAuthor = this.GetCountBook();
            Int32 countBooksInOtherAuthor = other.GetCountBook();
            Int32 comparacionBookCount = countBooksInThisAuthor.CompareTo(countBooksInOtherAuthor);
            return comparacionBookCount;
        }
        public override string ToString()
        {
            return FullName;
        }
    }
}
